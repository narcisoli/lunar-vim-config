lvim.lsp.automatic_servers_installation = false
lvim.log.level = "warn"
lvim.format_on_save = true
lvim.leader = "space"

-- lvim.builtin.dashboard.active = true
lvim.builtin.notify.active = true
lvim.builtin.terminal.active = true
lvim.builtin.nvimtree.setup.view.side = "left"
lvim.builtin.nvimtree.show_icons.git = 1
lvim.builtin.project.patterns = { ".git", "_darcs", ".hg", ".bzr", ".svn", "Makefile" }

lvim.builtin.treesitter.ensure_installed = {
	"bash",
	"c",
	"javascript",
	"json",
	"lua",
	"python",
	"typescript",
	"tsx",
	"css",
	"rust",
	"java",
	"yaml",
	"graphql",
	"svelte",
}

lvim.builtin.treesitter.ignore_install = { "haskell" }
lvim.builtin.treesitter.highlight.enabled = true

require("my-plugins")
require("my-which-key")
require("my-snip")
require("my-lsp")

-- lvim.colorscheme = "tokyonight"
-- lvim.builtin.lualine.options.theme = "tokyonight"
vim.opt.cmdheight = 1
vim.opt.timeoutlen = 400
vim.opt.relativenumber = false

-- termuinal
lvim.builtin.terminal.size = 90
lvim.builtin.terminal.execs = {
	{ "lazygit", "<leader>gg", "LazyGit", "tab" },
	{ "pter ~/todo.txt", "<leader>gt", "topydo columns", "float" },
	{ "spt", "<leader>g1", "Spotify", "float" },
	{ "lazygit", "<c-\\><c-g>", "LazyGit", "tab" },
}

-- dashboard
lvim.builtin.alpha.custom_header = {

	"                           ██████                                   ",
	"                         ████████████                               ",
	"                       ██████████████                               ",
	"     ██                ██████                                       ",
	"     ███               ███                                          ",
	"     ███               ██                                           ",
	"     ███               ██                                           ",
	"     ███               ██                                           ",
	"      ███              ██                                           ",
	"      █████           ███                                           ",
	"       █████          ██                                            ",
	"       ██████         ██                           ██               ",
	"        ███████       ██                                            ",
	"        ██  ████      ██                                            ",
	"        ██   ████     ██          ████             ██               ",
	" ██     ██    ████   ███   ████   █  █     █████   █      ██        ",
	"████    ██     ███   ███ ███ ████  ██████ ██   ██  █      ███       ",
	"████    ███     ███ ███ ██     ██   █  ██ █        █     ██ ██      ",
	" ███  █████     ███████ █      ██   █  █  █        █    ██   █      ",
	"  ████████       █████ ██      ██  ██ ██  █       ██   ██    █      ",
	"  ██████          ████  ██   ███████  ██ █████  ███████      █      ",
	"  █████            ███   █████        ███    ███    ██    ████      ",
	"                   ███                                              ",
}

-- telescope
lvim.builtin.telescope.defaults.layout_config.width = 0.95
lvim.builtin.telescope.defaults.layou_strategy = "flex"

lvim.colorscheme = "catppuccin"
