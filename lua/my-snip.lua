-- require("luasnip/loaders/from_vscode").load({
-- 	paths = { "~/.config/lvim/snippets/vscode-es7-javascript-react-snippets" },
-- })

-- require("luasnip/loaders/from_vscode").load({
--   paths = { "~/.config/lvim/snippets" },
-- })

local ls = require("luasnip")
ls.config.set_config({
  history = false,
  updateevents = "InsertLeave",
  delete_check_events = "InsertLeave",
  ext_base_prio = 300,
  ext_prio_increase = 1,
  -- ft_func = function()
  -- 	return vim.split(vim.bo.filetype, ".", true)
  -- end,
})
