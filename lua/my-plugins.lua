lvim.plugins = {
	-- "simrat39/symbols-outline.nvim",
	"wellle/targets.vim",
	-- "namadnuno/neoi18n",
	"andreypopp/vim-colors-plain",
	{
		"tpope/vim-surround",
		keys = { "c", "d", "y" },
	},
	{
		"nvim-telescope/telescope-ui-select.nvim",
		config = function()
			require("telescope").setup({
				extensions = {
					["ui-select"] = {
						require("telescope.themes").get_dropdown({
							-- even more opts
						}),

						-- pseudo code / specification for writing custom displays, like the one
						-- for "codeactions"
						-- specific_opts = {
						--   [kind] = {
						--     make_indexed = function(items) -> indexed_items, width,
						--     make_displayer = function(widths) -> displayer
						--     make_display = function(displayer) -> function(e)
						--     make_ordinal = function(e) -> string
						--   },
						--   -- for example to disable the custom builtin "codeactions" display
						--      do the following
						--   codeactions = false,
						-- }
					},
				},
			})
			-- To get ui-select loaded and working with telescope, you need to call
			-- load_extension, somewhere after setup function:
			require("telescope").load_extension("ui-select")
		end,
	},
	{
		"norcalli/nvim-colorizer.lua",
		config = function()
			require("colorizer").setup()
		end,
	},
	-- {
	--   "navarasu/onedark.nvim",
	-- },
	{
		"catppuccin/nvim",
		as = "catppuccin",
	},
	-- {
	--   "folke/trouble.nvim",
	--   cmd = "TroubleToggle",
	-- },
	-- {
	--   "weilbith/nvim-code-action-menu",
	--   cmd = "CodeActionMenu",
	-- },

	-- {
	--  "lukas-reineke/indent-blankline.nvim",
	--  -- your statusline
	--  config = function()
	--    require("indent_blankline").setup( {
	--     buftype_exclude = {
	--       "nofile",
	--       "terminal",
	--       "lsp-installer",
	--       "lspinfo",
	--     },
	--     filetype_exclude = {
	--       "help",
	--       "startify",
	--       "aerial",
	--       "alpha",
	--       "dashboard",
	--       "packer",
	--       "neogitstatus",
	--       "NvimTree",
	--       "neo-tree",
	--       "Trouble",
	--     },
	--     context_patterns = {
	--       "class",
	--       "return",
	--       "function",
	--       "method",
	--       "^if",
	--       "^while",
	--       "jsx_element",
	--       "^for",
	--       "^object",
	--       "^table",
	--       "block",
	--       "arguments",
	--       "if_statement",
	--       "else_clause",
	--       "jsx_element",
	--       "jsx_self_closing_element",
	--       "try_statement",
	--       "catch_clause",
	--       "import_statement",
	--       "operation_type",
	--     },
	--     show_trailing_blankline_indent = false,
	--     use_treesitter = true,
	--     char = "▏",
	--     show_current_context = true,
	--   })
	--  end,
	--  },
	-- {
	--   "beauwilliams/focus.nvim",
	--   config = function()
	--     require("focus").setup({
	--       excluded_filetypes = { "fugitiveblame", "harpoon" },
	--       number = true,
	--       -- colorcolumn = { enable = true, width = 100 },
	--       -- signcolumn = false,
	--       -- minwidth = 80,
	--       -- width = 100
	--     })
	--   end,
	-- },
	-- {
	--   "ziontee113/syntax-tree-surfer",
	--   event = "InsertEnter",
	--   config = function() end,
	-- },
	{
		"gbprod/yanky.nvim",
		event = "InsertEnter",
		config = function()
			require("yanky").setup({
				-- your configuration comes here
				-- or leave it empty to use the default settings
				-- refer to the configuration section below
			})
		end,
	},
	{
		"windwp/nvim-ts-autotag",
		event = "InsertEnter",
		config = function()
			require("nvim-ts-autotag").setup()
		end,
	},
	{ "tpope/vim-repeat" },
	-- {
	--   "folke/tokyonight.nvim",
	--   as = "tokyonight",
	--   config = function()
	--     -- vim.g.tokyonight_style = "night"
	--     -- vim.g.tokyonight_sidebars = { "qf" }
	--     -- vim.cmd("color tokyonight")
	--   end,
	-- },
	-- "RRethy/nvim-base16"  ,
	"leafgarland/typescript-vim",
	"peitalin/vim-jsx-typescript",
	{
		"folke/lsp-colors.nvim",
		event = "BufRead",
	},
	-- {
	-- 	"rlane/pounce.nvim",
	-- 	config = function()
	-- 		require("pounce").setup({
	-- 			accept_keys = "JFKDLSAHGNUVRBYTMICEOXWPQZ",
	-- 			accept_best_key = "<enter>",
	-- 			multi_window = false,
	-- 			debug = false,
	-- 		})
	-- 		vim.api.nvim_set_keymap("n", "S", ":PounceRepeat<cr>", { silent = true })
	-- 		vim.api.nvim_set_keymap("n", "s", ":Pounce<cr>", { silent = true })
	-- 	end,
	-- },
	{
		"phaazon/hop.nvim",
		event = "BufRead",
		config = function()
			require("hop").setup()
			vim.api.nvim_set_keymap("n", "s", ":HopWord<cr>", { silent = true })
			vim.api.nvim_set_keymap("n", "S", ":HopChar2<cr>", { silent = true })
		end,
	},
	-- {"folke/tokyonight.nvim"},
	"jose-elias-alvarez/nvim-lsp-ts-utils",
	-- {
	--   "akinsho/flutter-tools.nvim",
	--   config = function()
	--     require("flutter-tools").setup({
	--       widget_guides = {
	--         enabled = true,
	--       },
	--     })
	--   end,
	-- },
	{
		"ThePrimeagen/harpoon",
		config = function()
			require("harpoon").setup({
				global_settings = {
					save_on_toggle = true,
				},
				menu = {
					height = 20,
					width = 80,
				},
			})
		end,
	},
	{
		"monaqa/dial.nvim",
	},
	-- {
	-- 	"tzachar/cmp-tabnine",
	-- 	after = "nvim-cmp",
	-- 	run = "./install.sh",
	-- 	requires = "hrsh7th/nvim-cmp",
	-- },
}
-- local tabnine = require("cmp_tabnine.config")
-- tabnine:setup({
-- 	max_lines = 1000,
-- 	max_num_results = 20,
-- 	sort = true,
-- 	run_on_every_keystroke = true,
-- 	snippet_placeholder = "..",
-- 	ignored_file_types = { -- default is not to ignore
-- 		-- uncomment to ignore in lua:
-- 		-- lua = true
-- 	},
-- 	show_prediction_strength = false,
-- })

require("telescope").load_extension("harpoon")
local augend = require("dial.augend")
require("dial.config").augends:register_group({
	default = {
		augend.integer.alias.decimal,
		augend.integer.alias.hex,
		augend.date.alias["%Y/%m/%d"],
	},
	typescript = {
		augend.integer.alias.decimal,
		augend.integer.alias.hex,
		augend.constant.alias.bool,
		augend.constant.alias.bool,
		augend.date.alias["%Y/%m/%d"],
		augend.constant.new({
			elements = { "&&", "||" },
			word = false,
			cyclic = true,
		}),
		augend.constant.new({
			elements = { "!=", "==" },
			word = false,
			cyclic = true,
		}),
		augend.constant.new({ elements = { "let", "const" } }),
	},
	visual = {
		augend.integer.alias.decimal,
		augend.integer.alias.hex,
		augend.date.alias["%Y/%m/%d"],
		augend.constant.alias.alpha,
		augend.constant.alias.Alpha,
	},
})

-- change augends in VISUAL mode
vim.api.nvim_set_keymap("n", "<C-a>", require("dial.map").inc_normal("typescript"), { noremap = true })
vim.api.nvim_set_keymap("n", "<C-x>", require("dial.map").dec_normal("typescript"), { noremap = true })

vim.api.nvim_set_keymap(
	"x",
	"J",
	'<cmd>lua require("syntax-tree-surfer").surf("next", "visual")<cr>',
	{ noremap = true, silent = true }
)
vim.api.nvim_set_keymap(
	"x",
	"K",
	'<cmd>lua require("syntax-tree-surfer").surf("prev", "visual")<cr>',
	{ noremap = true, silent = true }
)
vim.api.nvim_set_keymap(
	"x",
	"H",
	'<cmd>lua require("syntax-tree-surfer").surf("parent", "visual")<cr>',
	{ noremap = true, silent = true }
)
vim.api.nvim_set_keymap(
	"x",
	"L",
	'<cmd>lua require("syntax-tree-surfer").surf("child", "visual")<cr>',
	{ noremap = true, silent = true }
)

vim.api.nvim_set_keymap("n", "p", "<Plug>(YankyPutAfter)", {})
vim.api.nvim_set_keymap("n", "P", "<Plug>(YankyPutBefore)", {})
vim.api.nvim_set_keymap("x", "p", "<Plug>(YankyPutAfter)", {})
vim.api.nvim_set_keymap("x", "P", "<Plug>(YankyPutBefore)", {})
vim.api.nvim_set_keymap("n", "gp", "<Plug>(YankyGPutAfter)", {})
vim.api.nvim_set_keymap("n", "gP", "<Plug>(YankyGPutBefore)", {})
vim.api.nvim_set_keymap("x", "gp", "<Plug>(YankyGPutAfter)", {})
vim.api.nvim_set_keymap("x", "gP", "<Plug>(YankyGPutBefore)", {})

vim.api.nvim_set_keymap("n", "<c-n>", "<Plug>(YankyCycleForward)", {})
vim.api.nvim_set_keymap("n", "<c-p>", "<Plug>(YankyCycleBackward)", {})

vim.api.nvim_set_keymap("n", "y", "<Plug>(YankyYank)", {})
vim.api.nvim_set_keymap("x", "y", "<Plug>(YankyYank)", {})
