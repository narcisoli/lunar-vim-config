local null_ls = require("null-ls")

local formatters = require("lvim.lsp.null-ls.formatters")
formatters.setup({
	{
		command = "prettier",
		-- extra_args = { "--print-width", "120" },
		filetypes = {
			"typescript",
			"liquid",
			"css",
			"scss",
			"typescriptreact",
			"svelte",
			"lua",
			"javascript",
			"graphql",
			"xhtml",
		},
		dynamic_command = require("null-ls.helpers.command_resolver").from_node_modules,
	},
})

-- local linters = require("lvim.lsp.null-ls.linters")
-- linters.setup({
-- 	-- { command = "black" },
-- 	{
-- 		command = "eslint_d",
-- 		---@usage specify which filetypes to enable. by default a providers will attach to all the filetypes it supports.
-- 		filetypes = { "typescript", "typescriptreact", "javascript", "javascriptreact" },
-- 	},
-- })

null_ls.setup({
	sources = {
		null_ls.builtins.formatting.stylua,
		-- null_ls.builtins.formatting.eslint,
	},
	-- diagnostics_format = "[#{c}] #{m} (#{s})",
})

local lspconfig = require("lspconfig")
lspconfig.svelte.setup({
	on_attach = function(client, bufnr)
		-- client.server_capabilities.document_formatting = false
		-- client.server_capabilities.document_range_formatting = false
	end,
	-- cmd = { "yarn", "svelteserver", "--stdio" },
	settings = {
		svelte = {
			plugin = {
				css = {
					enable = false,
				},
				svelte = {},
			},
		},
	},
})

lspconfig.cssls.setup({
	settings = {
		css = {
			lint = {
				unknownAtRules = "ignore",
			},
		},
	},
})
lspconfig.jsonls.setup({})
lspconfig.rust_analizer.setup({})
lspconfig.cssmodules_ls.setup({})
local capabilities = vim.lsp.protocol.make_client_capabilities()
-- capabilities.document_formatting = false
-- capabilities.document_range_formatting = false
-- lspconfig.emmet_ls.setup({
--     -- on_attach = on_attach,
--     capabilities = capabilities,
--     filetypes = { "html", "css", "typescriptreact", "javascriptreact" },
-- })
lspconfig.tailwindcss.setup({
	capabilities = capabilities,
	filetypes = {
		"aspnetcorerazor",
		"astro",
		"astro-markdown",
		"blade",
		"django-html",
		"edge",
		"eelixir",
		"ejs",
		"erb",
		"eruby",
		"gohtml",
		"haml",
		"handlebars",
		"hbs",
		"html",
		"html-eex",
		"heex",
		"jade",
		"leaf",
		"liquid",
		"mustache",
		"njk",
		"nunjucks",
		"php",
		"razor",
		"slim",
		"twig",
		"css",
		"less",
		"postcss",
		"sass",
		"scss",
		"stylus",
		"sugarss",
		"javascript",
		"javascriptreact",
		"reason",
		"rescript",
		"typescript",
		"typescriptreact",
		"vue",
		"svelte",
	},
})

lspconfig.dartls.setup({
	cmd = { "dart", "/usr/lib/dart/bin/snapshots/analysis_server.dart.snapshot", "--lsp" },
})

lspconfig.jsonls.setup({
	settings = {
		json = {
			schemas = require("schemastore").json.schemas(),
		},
	},
})
lspconfig.graphql.setup({

	-- arg= {}
	-- "settings": {
	--         "graphql-config.load.legacy": true
	--     }
	cmd = { "graphql-lsp", "server", "-m", "stream" },
	-- filetypes= {"typescript", "graphql"}
})
lspconfig.sumeko_lua.setup({})

lspconfig.tsserver.setup({

	cmd = { "typescript-language-server", "--stdio" },
	-- filetypes = { "typescript", "typescriptreact", "javascript", "graphql", "html"  },
	init_options = require("nvim-lsp-ts-utils").init_options,
	--
	on_attach = function(client, bufnr)
		client.server_capabilities.document_formatting = false
		client.server_capabilities.document_range_formatting = false
		local ts_utils = require("nvim-lsp-ts-utils")

		-- defaults
		ts_utils.setup({
			debug = false,
			disable_commands = false,
			enable_import_on_completion = true,

			-- import all
			import_all_timeout = 5000, -- ms
			-- lower numbers = higher priority
			import_all_priorities = {
				same_file = 1, -- add to existing import statement
				local_files = 2, -- git files or files with relative path markers
				buffer_content = 3, -- loaded buffer content
				buffers = 4, -- loaded buffer names
			},
			import_all_scan_buffers = 1000,
			import_all_select_source = true,
			-- if false will avoid organizing imports
			always_organize_imports = true,

			-- filter diagnostics

			-- inlay hints
			auto_inlay_hints = false,
			inlay_hints_highlight = "Comment",
			inlay_hints_priority = 200, -- priority of the hint extmarks
			inlay_hints_throttle = 150, -- throttle the inlay hint request

			-- update imports on file move
			update_imports_on_move = false,
			require_confirmation_on_move = true,
			watch_dir = nil,
		})

		ts_utils.setup_client(client)
	end,
})
