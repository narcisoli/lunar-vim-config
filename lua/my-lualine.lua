local conditions = require("lvim.core.lualine.conditions")
local colors = require("lvim.core.lualine.colors")

local function getPath()
	local z = vim.fn.fnamemodify(vim.fn.expand("%:h"), ":p:~:.")
	return z
end

local function getProjectName()
	local results = require("project_nvim").get_recent_projects()
	local minKey = 0
	local data = vim.fn.expand("%:p")
	local normalised_data = data:gsub("/", ""):gsub(" ", "")

	for k in pairs(results) do
		local name = results[k]
		local normalised_path = name:gsub("/", ""):gsub(" ", "")
		-- print(data)
		if string.find(normalised_data, normalised_path, nil, true) then
			-- print("A",normalised_data)
			-- print("B",normalised_path)
			if not results[minKey] or #results[minKey] < #results[k] then
				minKey = k
			end
		end
	end

	-- local z =vim.fn.fnamemodify(vim.fn.expand('%:h'), ':p:~:.')
	local name = vim.fn.fnamemodify(results[minKey], ":t")
	-- return z
	return name
end

local comp = {

	path = {
		getPath,
		color = { fg = "#ffaa88", gui = "italic" },
		cond = nil,
	},
	project = {
		getProjectName,
		color = function()
			return { fg = vim.bo.modified and "#aa3355" or "#33aa88", gui = "bold" }
		end,
		cond = conditions.hide_in_width,
	},
}
