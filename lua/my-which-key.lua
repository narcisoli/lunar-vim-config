local _, actions = pcall(require, "telescope.actions")
lvim.builtin.telescope.defaults.mappings = {
  -- for input mode
  i = {
    ["<C-j>"] = actions.move_selection_next,
    ["<C-k>"] = actions.move_selection_previous,
    ["<C-n>"] = actions.cycle_history_next,
    ["<C-p>"] = actions.cycle_history_prev,
    -- ["<C-p>"] = actions.hi,
    -- ["<C-k>"] = actions.move_selection_previous,
  },
}
lvim.builtin.which_key.mappings["j"] = { "<cmd>join<CR>", "Join" }
lvim.builtin.which_key.mappings["f"] = nil
lvim.builtin.which_key.mappings["p"] = nil

-- lvim.builtin.which_key.mappings["P"] = { "<cmd>Telescope projects<CR>", "Projects" }
-- lvim.builtin.which_key.mappings["p"] = { "<cmd>Telescope projects<CR>", "Projects" }
lvim.builtin.which_key.mappings["a"] = {
  name = "+Typescript",
  o = { "<cmd>TSLspOrganize<cr>", "Organize Imports" },
  a = { "<cmd>TSLspImportAll<cr>", "Import All" },
  r = { "<cmd>TSLspRenameFile<cr>", "RenameFile" },
}
lvim.keys.normal_mode = { -- Page down/up
  ["[d"] = "<PageUp>",
  ["]d"] = "<PageDown>",

  -- Navigate buffers
  ["<Tab>"] = ":lua require('harpoon.ui').nav_next() <cr>",
  ["<S-Tab>"] = ":lua require('harpoon.ui').nav_prev() <cr>",
  ["gh"] = ":lua vim.lsp.buf.hover()<CR>",
  ["gr"] = ":Telescope lsp_references<CR>",
  ["<S-k>"] = ":lua vim.lsp.buf.hover()<CR>",
  ["<C-s>"] = ":w<cr>",
  ["gd"] = ":Telescope lsp_definitions<CR>",
  -- ["gD"] = { "<cmd>lua vim.lsp.buf.declaration()<CR>", "Goto declaration" },
  -- ["gr"] = { "<cmd>Telescope lsp_references<CR>", "Goto references" },
}

lvim.builtin.which_key.mappings["k"] = {
  name = "+Flutter",
  f = { "<cmd>FlutterRun<cr>", "Run" },
  r = { "<cmd>FLutterReload<cr>", "Reload" },
  R = { "<cmd>FlutterRestart<cr>", "Restart" },
  c = { "<cmd>FlutterLogClear<cr>", "Log clear" },
  o = { "<cmd>FlutterOutline<cr>", "Outline" },
  t = { "<cmd>FlutterOutlineToggle<cr>", "Toggle Outline" },
  q = { "<cmd>FlutterQuit<cr>", "Quit" },
  d = { "<cmd>FlutterDevTools<cr>", "Dev Tools" },
  u = { "<cmd>FlutterPubGet<cr>", "Pub get" },
  e = { "<cmd>FlutterDevices<cr>", "Devices" },
}

lvim.builtin.which_key.mappings["s"] = {
  name = "Search",
  b = { "<cmd>Telescope git_branches<cr>", "Checkout branch" },
  c = { "<cmd>Telescope grep_string<cr>", "Current word" },
  f = { "<cmd>Telescope find_files<cr>", "Find Project Files" },
  g = { require("lvim.core.telescope.custom-finders").find_project_files, "Find Git Files" },
  H = { "<cmd>Telescope help_tags<cr>", "Find Help" },
  h = { "<cmd>Telescope search_history<cr>", "History" },
  M = { "<cmd>Telescope man_pages<cr>", "Man Pages" },
  o = { "<cmd>Telescope jump_list<cr>", "Jump List" },
  r = { "<cmd>Telescope oldfiles<cr>", "Open Recent File" },
  R = { "<cmd>Telescope registers<cr>", "Registers" },
  t = { "<cmd>Telescope live_grep<cr>", "Text" },
  k = { "<cmd>Telescope keymaps<cr>", "Keymaps" },
  C = { "<cmd>Telescope commands<cr>", "Commands" },
  m = { "<cmd>Telescope marks<cr>", "Marks" },
  p = { "<cmd>Telescope projects<cr>", "Projects" },
  P = {
    "<cmd>lua require('telescope.builtin.internal').colorscheme({enable_preview = true})<cr>",
    "Colorscheme with Preview",
  },
}
lvim.builtin.which_key.mappings["t"] = {
  name = "Diagnostics",
  t = { "<cmd>TroubleToggle<cr>", "trouble" },
  w = { "<cmd>TroubleToggle workspace_diagnostics<cr>", "workspace" },
  d = { "<cmd>TroubleToggle document_diagnostics<cr>", "document" },
  q = { "<cmd>TroubleToggle quickfix<cr>", "quickfix" },
  l = { "<cmd>TroubleToggle loclist<cr>", "loclist" },
  r = { "<cmd>TroubleToggle lsp_references<cr>", "references" },
}

-- lvim.builtin.which_key.mappings["J"] = { "<cmd>:+5<cr>", "NOH" }
-- lvim.builtin.which_key.mappings["K"] = { "<cmd>:noh<cr>", "NOH" }
-- vim.api.nvim_set_keymap("n", "<C-k>", "<cmd>:-9<cr>", {})
-- vim.api.nvim_set_keymap("n", "<C-j>", "<cmd>:+9<cr>", {})
-- vim.api.nvim_set_keymap("n", "J", "<cmd>:+5<cr>", {})
-- vim.api.nvim_set_keymap("n", "P", "<Plug>(YankyPutBefore)", {})
-- ["K"] = { "<cmd>lua vim.lsp.buf.hover()<CR>", "Show hover" },

lvim.builtin.which_key.mappings["l"]["R"] = { "<cmd>:LspRestart<cr>", "Restart" }
-- lvim.builtin.which_key.mappings["l"]["a"] = { "<cmd>:CodeActionMenu<cr>", "Action" }
lvim.builtin.which_key.mappings["."] = { "<cmd>:noh<cr>", "NOH" }
lvim.builtin.which_key.mappings["E"] = { "<cmd>NvimTreeFocus<CR>", "Explorer" }
lvim.builtin.which_key.mappings["."] = { "<cmd>:noh<cr>", "NOH" }

lvim.builtin.which_key.mappings["h"] = { "<cmd>:lua require('harpoon.ui').toggle_quick_menu()<cr>", "Harpoon show" }
lvim.builtin.which_key.mappings["m"] = { "<cmd>:lua require('harpoon.mark').add_file()<cr>", "Harpoon save" }
lvim.builtin.which_key.mappings["H"] = { "<cmd>:lua require('harpoon.mark').add_file()<cr>", "Harpoon save" }
lvim.builtin.which_key.mappings["1"] = { "<cmd>:lua require('harpoon.ui').nav_file(1)<cr>", "Go to 1" }
lvim.builtin.which_key.mappings["2"] = { "<cmd>:lua require('harpoon.ui').nav_file(2)<cr>", "Go to 2" }
lvim.builtin.which_key.mappings["3"] = { "<cmd>:lua require('harpoon.ui').nav_file(3)<cr>", "Go to 3" }
lvim.builtin.which_key.mappings["4"] = { "<cmd>:lua require('harpoon.ui').nav_file(4)<cr>", "Go to 4" }
lvim.builtin.which_key.mappings["5"] = { "<cmd>:lua require('harpoon.ui').nav_file(5)<cr>", "Go to 5" }
